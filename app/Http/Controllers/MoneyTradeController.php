<?php

namespace App\Http\Controllers;

use App\MoneyTrade;
use Illuminate\Http\Request;



class MoneyTradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.mt-registration');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'bank' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'mt_first_name' => ['required', 'string', 'max:255'],
            'mt_last_name' => ['required', 'string', 'max:255'],
            'mt_password' => ['required', 'string', 'min:8', 'confirmed'],
            'mt_account' => ['required', 'string', 'max:255'],
            'mt_deposit' => ['required', 'string', 'max:255'],
            'mt_leverage' => ['required', 'string', 'max:255'],


        ]);

        $moneyTrade = new MoneyTrade();

        $moneyTrade->mt_number = uniqid('MTNumber-');

        $moneyTrade->bank = $request->input('bank');
        $moneyTrade->email = $request->input('email');
        $moneyTrade->mt_first_name = $request->input('mt_first_name');
        $moneyTrade->mt_last_name = $request->input('mt_last_name');
        $moneyTrade->mt_password = $request->input('mt_password');
        $moneyTrade->mt_account = $request->input('mt_account');
        $moneyTrade->mt_deposit = $request->input('mt_deposit');
        $moneyTrade->mt_leverage = $request->input('mt_leverage');

        $moneyTrade->user_id = auth()->id();

        $moneyTrade->save();

        return redirect()->route('home')->withMessage('MoneyTrade Account Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MoneyTrade  $moneyTrade
     * @return \Illuminate\Http\Response
     */
    public function show(MoneyTrade $moneyTrade)
    {
        return view('home.money-app');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MoneyTrade  $moneyTrade
     * @return \Illuminate\Http\Response
     */
    public function edit(MoneyTrade $moneyTrade)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MoneyTrade  $moneyTrade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MoneyTrade $moneyTrade)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MoneyTrade  $moneyTrade
     * @return \Illuminate\Http\Response
     */
    public function destroy(MoneyTrade $moneyTrade)
    {
        //
    }
}
